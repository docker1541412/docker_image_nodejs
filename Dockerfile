# Собираем приложение
FROM node:16-alpine AS builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

# Создаем lightweight образ
FROM node:16-alpine

WORKDIR /app
COPY --from=builder /app/dist ./

# Устанавливаем http-server
RUN npm install -g http-server

# Открываем порт
EXPOSE 8080

# Запускаем приложение
CMD ["http-server", "-p", "8080"]
